//
//  Imagur.swift
//  ImageDev
//
//  Created by Mac Informatica  on 28/01/22.
//

import Foundation
import UIKit


struct Image {
    static let bhaktapur_darbar: UIImage = UIImage(named: "bhaktapur darbar.jpg")!
    static let bhaktapur: UIImage = UIImage(named: "bhaktapur.jpeg")!
    static let everest: UIImage = UIImage(named: "everest base camp.jpg")!
    static let jomsom: UIImage = UIImage(named: "jomsom.jpg")!
    static let MtEverest: UIImage = UIImage(named: "MtEverest.jpg")!
    static let MtFishTail: UIImage = UIImage(named: "MtFishTail.jpeg")!
    static let muktinath: UIImage = UIImage(named: "muktinath.jpg")!
    static let pasupatinath: UIImage = UIImage(named: "pasupatinath.jpg")!
    static let patan: UIImage = UIImage(named: "patan darbar.jpg")!
    static let rara: UIImage = UIImage(named: "rara lake.jpg")!
    static let tilicho: UIImage = UIImage(named: "tilicho.jpg")!
    static let mustang: UIImage = UIImage(named: "upper mustang.jpeg")!
}

extension UIViewController {

    func pushView(viewController: UIViewController) {
        let transition = CATransition()
        transition.duration = 0.3
        transition.type = CATransitionType.fade
        self.view.window!.layer.add(transition, forKey: kCATransition)
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    
    func dismissView() {
        let transition = CATransition()
        transition.duration = 0.3
        transition.type = CATransitionType.reveal
        transition.subtype = CATransitionSubtype.fromBottom
        self.view.window!.layer.add(transition, forKey: kCATransition)
        navigationController!.popViewController(animated: true)
    }
}







/*
extension UIView {

    func  addTapGesture(action : @escaping ()->Void ){
        let tap = TapGestureRecognizer(target: self , action: #selector(self.handleTap(_:)))
        tap.action = action
        tap.numberOfTapsRequired = 1

        self.addGestureRecognizer(tap)
        self.isUserInteractionEnabled = true
    }

    @objc func handleTap(_ sender: TapGestureRecognizer) {
        sender.action!()
    }
}


class TapGestureRecognizer: UITapGestureRecognizer {
    var action : (()->Void)? = nil
}
*/


