//
//  Imagur.swift
//  ImageDev
//
//  Created by Mac Informatica  on 28/01/22.
//

import Foundation
class ApiImagur{
    class func makeRequest(completion: @escaping(Root)->()){
        var url = URLRequest(url: URL(string: "https://api.imgur.com/3/account/me/images")!,timeoutInterval: Double.infinity)
        url.addValue("Bearer d46cd25871560ea06e53b24f3fbdc5618965538c", forHTTPHeaderField: "Authorization")
        
        let task = URLSession.shared.dataTask(with: url) { data, response, error in
            guard let responseData = data else { return }
            do {
                let hsAlbum = try JSONDecoder().decode(Root.self, from: responseData)
                    completion(hsAlbum)
            } catch let error {
                print("Error: \(error)")
            }
        }
        task.resume()
    }
}
